<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InterviewQuestion extends Model
{
    protected $table = 'interview_questions';
}

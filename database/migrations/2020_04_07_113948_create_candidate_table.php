<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCandidateTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('candidate', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name', 100);
            $table->string('email', 100);
            $table->string('mobile', 100);
            $table->string('current_loaction', 100);
            $table->string('cv', 191);
            $table->string('highest_qualification', 100);
            $table->string('course', 100);
            $table->string('specialization', 100);
            $table->string('college', 191);
            $table->string('course_type', 100)->comment('full-time,part-time');
            $table->integer('passing_year');
            $table->string('current_designation', 100)->nullable();
            $table->string('current_company', 100)->nullable();
            $table->integer('annual_salary')->nullable();
            $table->string('total_experience', 100)->nullable();
            $table->string('notice_period', 100)->nullable();
            $table->string('skills', 100)->nullable();
            $table->string('industry', 100)->nullable();
            $table->string('functional_area', 100)->nullable();
            $table->string('role', 100)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('candidate');
    }
}
